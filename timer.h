/*
frigOS
Licensed under GPLv3
*/
#ifndef __TIMER_H__
#define __TIMER_H__

#include <stdint.h>

extern volatile uint16_t ticks1;
extern volatile uint16_t ticks3;
void initWatchdog(void);
void initTimer1(void);
void initTimer2(void);
void initTimer3(void);

#endif
