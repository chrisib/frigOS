/*
frigOS
Licensed under GPLv3
*/
#include "protocol.h"
#include "serial.h"
#include "shared_memory.h"

uint8_t protocolCheckSum(uint8_t const buffer[], uint8_t buffer_length) {

	uint16_t result = 0;
	uint8_t x;
	for (x = 0; x < buffer_length; x++)
		result += buffer[x];

	return (uint8_t) (result & 0x7f);

} // protocolCheckSum

void protocolSuccess(void) {

	uint8_t reply[3];

	reply[0] = CMD_HEADER;
	reply[1] = 0;
	reply[2] = protocolCheckSum(&reply[1], 1);

	lockSemaphore(&usart1_tx_lock);
	transmitBufferUSART1(reply, 3);
	unlockSemaphore(&usart1_tx_lock);

	/*uint8_t reply[8];

	reply[0] = 'a';
	reply[1] = 'b';
	reply[2] = 'c';
	reply[3] = 'd';
	reply[4] = 'e';
	reply[5] = 'f';
	reply[6] = 'g';
	reply[7] = 'h';

	lockSemaphore(&usart1_lock);
	transmitBufferUSART1(reply, 8);
	unlockSemaphore(&usart1_lock);*/


} // protocolSuccess

void protocolFail(void) {

	uint8_t reply[3];

	reply[0] = CMD_HEADER;
	reply[1] = 1;
	reply[2] = protocolCheckSum(&reply[1], 1);

	lockSemaphore(&usart1_tx_lock);
	transmitBufferUSART1(reply, 3);
	unlockSemaphore(&usart1_tx_lock);

} // protocolFail
