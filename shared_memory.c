/*
frigOS
Licensed under GPLv3
*/

#include "shared_memory.h"
#include "serial.h"
#include "kernel.h"

/*!
 * \brief Shared I/O buffer for USART0 Rx
 */
volatile uint8_t shared_usart0_rx_buffer[USART0_RX_BUFFER_SIZE];

/*!
 * \brief length of the shared USART0 Rx buffer
 */
volatile uint8_t shared_usart0_rx_buffer_length;

/*!
 * \brief Shared I/O buffer for USART1 Rx
 */
volatile uint8_t shared_usart1_rx_buffer[USART1_RX_BUFFER_SIZE];

/*!
 * \brief length of the shared USART1 Rx buffer
 */
volatile uint8_t shared_usart1_rx_buffer_length;

/*!
 * \brief Semaphore to lock the USART0 Rx buffer
 */
volatile Semaphore shared_usart0_rx_buffer_lock;

/*!
 * \brief Semaphore to lock the USART1 Rx buffer
 */
volatile Semaphore shared_usart1_rx_buffer_lock;

/*!
 * \brief Initialize the shared Rx buffer memory and associated semaphores
 */
void initSharedMemory() {
    initSemaphore(&shared_usart0_rx_buffer_lock, 1);
    initSemaphore(&shared_usart1_rx_buffer_lock, 1);

    shared_usart0_rx_buffer_length = 0;
    shared_usart1_rx_buffer_length = 0;

}
