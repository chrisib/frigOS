#ifndef DISABLE_AXS1

#ifndef __AXS1_H__
#define __AXS1_H__

#include <stdint.h>
#include "defs.h"
#include "dynamixel.h"

/*!
 * \brief Address table for the AX-S1 IR/Microphone sensor
 */
enum AXS1_ADDRESS
{
    AXS1_MODEL_NUMBER_L            = 0x00,
    AXS1_MODEL_NUMBER_H            = 0x01,
    AXS1_VERSION_OF_FIRMWARE       = 0x02,
    AXS1_ID                        = 0x03,
    AXS1_BAUD_RATE                 = 0x04,
    AXS1_RETURN_DELAY_TIME         = 0x05,
    AXS1_HIGHEST_LIMIT_TEMPERATURE = 0x0b,
    AXS1_LOWEST_LIMIT_VOLTAGE      = 0x0c,
    AXS1_HIGHEST_LIMIT_VOLTAGE     = 0x0d,
    AXS1_STATUS_RETURN_LEVEL       = 0x10,
    AXS1_OBSTACLE_DETECT_COMPARE_V = 0x14,
    AXS1_LIGHT_DETECT_COMPARE_V    = 0x15,
    AXS1_LEFT_IR_SENSOR            = 0x1a,
    AXS1_CTR_IR_SENSOR             = 0x1b,
    AXS1_RIGHT_IR_SENSOR           = 0x1c,
    AXS1_LEFT_LUMINOSITY           = 0x1d,
    AXS1_CTR_LUMINOSITY            = 0x1e,
    AXS1_RIGHT_LUMINOSITY          = 0x1f,
    AXS1_OBSTACLE_DETECT_FLAG      = 0x20,
    AXS1_LUMINOSITY_DETECT_FLAG    = 0x21,
    AXS1_SOUND_DATA                = 0x23,
    AXS1_SOUND_DATA_MAX_HOLD       = 0x24,
    AXS1_SOUND_DETECTED_MAX_COUNT  = 0x25,
    AXS1_SOUND_DETECTED_TIME_LO    = 0x26,
    AXS1_SOUND_DETECTED_TIME_HI    = 0x27,
    AXS1_BUZZER_INDEX              = 0x28,
    AXS1_BUZZER_TIME               = 0x29,
    AXS1_PRESENT_VOLTAGE           = 0x2a,
    AXS1_PRESENT_TEMPERATURE       = 0x2b,
    AXS1_REGISTERED_INSTRUCTION    = 0x2c,
    AXS1_IR_REMOCON_ARRIVED        = 0x2e,
    AXS1_LOCK                      = 0x2f,
    AXS1_IR_REMOCON_RX_DATA0       = 0x30,
    AXS1_IR_REMOCON_RX_DATA1       = 0x31,
    AXS1_IR_REMOCON_TX_DATA0       = 0x32,
    AXS1_IR_REMOCON_TX_DATA1       = 0x33,
    AXS1_OBSTACLE_DETECT_COMPARE   = 0x34,
    AXS1_LIGHT_DETECT_COMPARE      = 0x35
};


#endif // __AXS1_H__
#endif DISABLE_AXS1
