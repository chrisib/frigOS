TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.c \
    usart1.c \
    usart0.c \
    timer.c \
    shared_memory.c \
    serial.c \
    protocol.c \
    led.c \
    kernel.c \
    dynamixel.c \
    cm5.c \
    button.c \
    axs1.c \
    ax12.c \
    adc.c

OTHER_FILES += \
    README \
    Makefile \
    LICENSE

HEADERS += \
    usart1.h \
    usart0.h \
    timer.h \
    shared_memory.h \
    serial.h \
    protocol.h \
    led.h \
    kernel.h \
    fixed_point.h \
    dynamixel.h \
    defs.h \
    cm5.h \
    button.h \
    axs1.h \
    ax12.h \
    adc.h

