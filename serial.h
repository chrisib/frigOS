/*
frigOS
Licensed under GPLv3

This file contains defintitions for using the two serial ports
found on the AtMega128 family of processors.

USART0 is assumed to be connected to the Bioloid's AX-12 motors
(see ax12motor.h), and is half-duplex.

USART1 is assumed to be connected to a PC and is full-duplex.
*/

#ifndef __SERIAL_H__
#define __SERIAL_H__

#include "defs.h"
#include "kernel.h"

#include <avr/io.h>

// --> START TODO: unnecessary once code rewrites are complete
extern uint8_t reply_status __attribute__((deprecated("")));
extern uint8_t reply_valid __attribute__((deprecated("")));
enum ReplyStatus
{

    REPLY_OFF,
    REPLY_ON
};
static inline uint8_t getReplyStatus(void) __attribute__((deprecated("")))
{
    return reply_status;
};
static inline void setReplyStatus(uint8_t status) __attribute__((deprecated("")))
{
    reply_status = status;
};
// --> END TODO

/*!
 * \brief The port an IO_Queue transmits on
 */
enum TransmitOnSerialPort
{
    TX_ON_NONE,     // queue is receive-only
    TX_ON_USART0,   // queue sends over USART0
    TX_ON_USART1    // queue sends over USART1
};

// circular queue used by Rx and Tx interrupts to buffer the I/O
// data should be inserted and extracted by the enqueue and dequeue
// functions below
/*!
 * \brief A circular queue used by Rx and Tx interrupts to buffer I/O
 *
 * Data is inserted and extracted using the enqueue and dequeue functionss
 * \see enqueue
 * \see dequeue
 */
typedef struct io_queue
{
    uint8_t maxLength;          // the maximum length of the queue
    uint8_t queueLength;        // the current number of bytes in the queue
    volatile uint8_t* buffer;   // pointer to an array of length maxLength
    volatile uint8_t in;        // the index of back of the queue
    volatile uint8_t out;       // the index of the front of the queue
    volatile BOOL empty;        // is the queue empty right now?
    volatile BOOL full;         // is the queue full right now?
    volatile BOOL txOnEnqueue;  // next time we enqueue a byte do we need to synthetically fire a Tx interrupt to restart the interrupt-driven I/O?
    volatile uint8_t txOn;      // what serial port does this queue transmit over?
} IO_Queue;


void enqueue(uint8_t data, volatile IO_Queue *queue);
uint8_t dequeue(volatile IO_Queue *queue);


// automagically include USART0/1 headers if serial.h is included
#include "usart0.h"
#include "usart1.h"


void initRS485(void);
char nibble2char(uint8_t nibble);
void byte2hex(uint8_t src, char dest[]);

#endif  //__SERIAL_H__
