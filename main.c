/*
frigOS
Licensed under GPLv3
*/
#include "avr/interrupt.h"
#include "util/delay.h"
#include "adc.h"
#include "ax12.h"
#include "button.h"
#include "defs.h"
#include "fixed_point.h"
#include "kernel.h"
#include "led.h"
#include "protocol.h"
#include "serial.h"
#include "shared_memory.h"
#include "timer.h"

/*!
 * \brief The main thread
 * \param arg
 */
void mainThread(void* arg)
{
    for(;;)
    {
        /*
        waitTimeout(UP_BUTTON_PRESSED_EVENT,500);

        if(thread[currentThread].waitingFor & UP_BUTTON_PRESSED_EVENT)
        {
            transmitStringUSART1_busyLoop("Up\n");
            changeLED(AUX_LED, LED_TOGGLE);
        }
        else
        {
            changeLED(PLAY_LED, LED_TOGGLE);
            transmitStringUSART1_busyLoop("Timeout\n");
        }
        */

        if(pollButtons() & UP_BUTTON)
            changeLED(AUX_LED, LED_TOGGLE);

        sleep(10);
    }
}

/*!
 * \brief Main entry point for the program
 * \return Should never, ever return
 *
 * Boots the kernel, starts the main thread and I/O threads
 */
int main(void)
{
#ifndef HW_DEBUG
    cli();

    // boot frigOS
    boot();


#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("starting monitor threads...");
#endif

    // start the monitor threads here
#ifndef DISABLE_BUTTONS
    // handles button events; check every 10ms and fire the button events if/when necessary
    startMonitorThread( &monitorButtons, 10);
#endif

#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#endif


#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("starting main threads...");
#endif

    startThread( (void (*)(void*)) &monitorStack, (void*) 0, MINIMUM_PRIORITY, MINIMUM_STACK_SIZE);
    startThread( (void (*)(void*)) &monitorUSART0, (void*) 0, MEDIUM_PRIORITY, MINIMUM_STACK_SIZE);
    startThread( (void (*)(void*)) &monitorUSART1, (void*) 0, MEDIUM_PRIORITY, MINIMUM_STACK_SIZE);

    // start the main tasks here
    //startThread( (void (*)(void*)) &mainThread, (void*) 0, MEDIUM_PRIORITY, MINIMUM_STACK_SIZE);

#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");

    transmitStringUSART1_busyLoop("\nboot successful\n");
#endif


    // enable interrupts so that the task-switcher can do its thing
    sei();

#else   // HW_DEBUG

    //testMotors();

    initButtons();

    for(;;)
    {
        if(upButtonPressed())
            changeLED(AUX_LED,LED_TOGGLE);

        changeLED(POWER_LED,LED_TOGGLE);
        _delay_ms(250);
    }
#endif

    // main should never, ever return
    for(;;);
}
