/*
frigOS
Licensed under GPLv3
*/

#ifndef __SHARED_MEMORY_H__
#define __SHARED_MEMORY_H__

#include "kernel.h"

extern volatile uint8_t shared_usart0_rx_buffer[];
extern volatile uint8_t shared_usart0_rx_buffer_length;

extern volatile uint8_t shared_usart1_rx_buffer[];
extern volatile uint8_t shared_usart1_rx_buffer_length;

extern volatile Semaphore shared_usart0_rx_buffer_lock;
extern volatile Semaphore shared_usart1_rx_buffer_lock;

void initSharedMemory(void);

#endif  //__SHARED_MEMORY_H__
