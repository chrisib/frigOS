// general-purpose definitions that don't really belong anywhere else
#ifndef __DEFS_H__
#define __DEFS_H__

// Freezer OS version information
// generally unused, but might be useful if we ever support
// querying the kernel information from the client
#define OS_VERSION     "2011.05.06"
#define OS_NAME        "frigOS"

// make booleans a uint8_t, define true and false
#define BOOL uint8_t
#define TRUE 1
#define FALSE 0

// define NULL for use with strings
#define NULL 0

// define max and min functions as macros
#define MAX(x,y) (((x)>=(y)) ? (x) : (y))
#define MIN(x,y) (((x)<=(y)) ? (x) : (y))

#endif
