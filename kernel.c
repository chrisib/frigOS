/*
frigOS
Licensed under GPLv3
*/
#include "kernel.h"
#include "serial.h"
#include "timer.h"
#include "adc.h"
#include "led.h"
#include "button.h"
#include "cm5.h"

#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

// kernel status variables & arrays
// names should make them self explanatory
volatile TCB thread[MAXIMUM_TASKS];
volatile MCB monitorThread[MAXIMUM_MONITOR_THREADS];
volatile uint8_t stack[STACK_SIZE];
volatile uint8_t currentThread;
volatile uint8_t lastNonIdleThread;
volatile uint8_t totalThreads;
volatile uint8_t tidFree;
volatile uint8_t lastMonitorThread;
volatile uint8_t totalMonitorThreads;


// flag of events fired since the last tick
volatile uint16_t pendingEvents;

Partition partitionList[] = {
    { (void *) &stack[RAIL0 - 1], IDLE_STACK_SIZE, FREE },
    { (void *) &stack[RAIL1 - 1], MINIMUM_STACK_SIZE, FREE },
    { (void *) &stack[RAIL2 - 1], MINIMUM_STACK_SIZE, FREE },
    { (void *) &stack[RAIL3 - 1], MINIMUM_STACK_SIZE, FREE },
    { (void *) &stack[RAIL4 - 1], MINIMUM_STACK_SIZE, FREE },
    { (void *) &stack[RAIL5 - 1], MEDIUM_STACK_SIZE, FREE },
    { (void *) &stack[RAIL6 - 1], MEDIUM_STACK_SIZE, FREE },
    { (void *) &stack[RAIL7 - 1], MAXIMUM_STACK_SIZE, FREE }
};

/*!
 * \brief Initialize the MCB structures so we can run monitor threads
 */
void initMonitorThreads(void)
{
    // no micro threads yet
    totalMonitorThreads = 0;
    lastMonitorThread = 0;

    uint8_t i;
    for(i=0; i<MAXIMUM_MONITOR_THREADS; i++)
    {
        monitorThread[i].execute = NULL;
        monitorThread[i].interval = 0;
        monitorThread[i].count = 0;
        monitorThread[i].tid = NO_MONITOR_THREAD_SCHEDULED;
        monitorThread[i].parentTid = IDLE;
        monitorThread[i].jmpBuf = NULL;
    }
}

/*!
 * \brief Initialize the kernel; set up TCBs and stack space, configure the timer we use
 * to trigger context switches.
 * Starts the IDLE thread.  If compiled with the KERNEL_DEBUG flag we also start a minumum-priority,
 * min-stack-size monitor task to check for overflows
 */
void initKernel(void)
{
    pendingEvents = 0x00;
    totalThreads = 0;
    tidFree = 0;
    sinit();

    startThread( (void(*)(void*)) &idle, 0, IDLE_PRIORITY, IDLE_STACK_SIZE );

#ifdef KERNEL_DEBUG

    startThread( (void (*)(void*)) &monitorStack, 0, MINIMUM_PRIORITY, MINIMUM_STACK_SIZE );

#endif

    // Set an invalid task for now
    currentThread = MAXIMUM_TASKS;
}

// print a nice welcome message over USART1
// note that this uses busy-loops instead of interrupt-driven I/O
// since during boot interrupts are disabled
/*!
 * \brief Print a boot message in plain ASCII over USART1 with the OS vesion info
 *
 * This uses busy-loops for the serial I/O since interrupts are disabled during boot
 */
void printBootBanner()
{
    transmitStringUSART1_busyLoop("\n+++++ ");
    transmitStringUSART1_busyLoop(OS_NAME);
    transmitStringUSART1_busyLoop(" v");
    transmitStringUSART1_busyLoop(OS_VERSION);
    transmitStringUSART1_busyLoop(" +++++\n\n");
}

/*!
 * \brief Initialize the FrigOS kernel; initializes the kernel, hardware (USART0, USART1, buttons, adc).
 *
 * Should be called from main before attempting to start any threads or monitor threads.
 *
 * Calls the following in-order:
 * - initUSART1
 * - printBootBanner (if compiled without the QUIET_BOOT flag)
 * - initRS485 (needed for USART0)
 * - initUSART0
 * - initLEDs
 * - initADC
 * - initCM5
 * - initButtons
 * - initKernel
 * - initMonitorThreads
 * - initTimer3
 * - initTimer1
 *
 * Finally, if compiled without NO_LED_FLASH, all LEDs on the board will flash
 * to indicate that the boot process finishes
 */
void boot(void)
{
    // always initialize USART1 so we can send/receive debugging information & boot messages
    initUSART1(USART1_DEFAULT_BAUD, FALSE);

#ifndef QUIET_BOOT
    printBootBanner();
    transmitStringUSART1_busyLoop("initializing hardware...\n");
#endif

    // initialize RS485 (needed for USART0)
#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("\t- RS485...");
#endif
    initRS485();
#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#endif

    // initialize USART0
#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("\t- USART0...");
#endif
    initUSART0();
#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#endif

    // initialize LEDs
#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("\t- LEDs...");
#endif
    initLEDs();
#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#endif

    // initialize ADC
#ifndef DISABLE_ADC
#  ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("\t- ADC...");
#  endif
    initADC();
#  ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#  endif
#endif

#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("\t- CONTROL TABLE...");
#endif
    initCM5();
#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#endif

    // initialize state (may not be necessary later on)
//#ifndef QUIET_BOOT
//    transmitStringUSART1_busyLoop("\t- state...");
//#endif
//    initState();
//#ifndef QUIET_BOOT
//    transmitStringUSART1_busyLoop(" [success]\n");
//#endif

    // initialize hardware buttons
#ifndef DISABLE_BUTTONS
#  ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("\t- buttons...");
#  endif
    initButtons();
#  ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#  endif
#endif

#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop("done\n");

    transmitStringUSART1_busyLoop("starting kernel...");
#endif

    // start up the kernel & related timers
    initKernel();
    initMonitorThreads();
    initTimer3();
    initTimer1();

#ifndef QUIET_BOOT
    transmitStringUSART1_busyLoop(" [success]\n");
#endif
#ifndef NO_LED_FLASH
    cli();
    changeAllLEDs(LED_ON);
    _delay_ms(250);
    changeAllLEDs(LED_OFF);
    _delay_ms(250);
    changeAllLEDs(LED_ON);
    _delay_ms(250);
    changeAllLEDs(LED_OFF);
#endif
}

/*!
 * \brief Start a new thread
 * \param functionPtr A function to start the thread with
 * \param priority The thread's priority
 * \param stackSize The desired stack size for the thread
 * \return True if the thread was started successfully, otherwise false
 */
uint8_t startThread(void (*functionPtr)(void *arg), void *arg, uint8_t priority, uint16_t stackSize)
{
    uint8_t result = FALSE;
    uint8_t id;

    cli();
    id = talloc();

    if (id != MAXIMUM_TASKS)
    {
        thread[id].tid = id;
        thread[id].state = READY;
        thread[id].priority = priority;
        thread[id].sleep = 0;
        thread[id].niceness = 125;
        thread[id].monitorLevel = 0;

        if ( ( thread[id].stackPtr  = salloc(stackSize) ) != NULL )
        {
            // create illusion new task was just interrupted
            uint16_t stackPtr = SP;
            SP = (uint16_t) thread[id].stackPtr;

            asm volatile(
                "push %0\n\t"	// PC - low byte (stop function)
                "push %1\n\t"	// PC - high byte
                "push %2\n\t"	// PC - low byte (start function)
                "push %3\n\t"	// PC - high byte
                "push __zero_reg__\n\t"	// __zero_reg__
                "push __tmp_reg__\n\t"	// __tmp_reg__
                "in __tmp_reg__,__SREG__\n\t"
                "push __tmp_reg__\n\t"	// SREG
                "in __tmp_reg__,0x3b\n\t"
                "push __tmp_reg__\n\t"	// RAMPZ
                "push r2\n\t"
                "push r3\n\t"
                "push r4\n\t"
                "push r5\n\t"
                "push r6\n\t"
                "push r7\n\t"
                "push r8\n\t"
                "push r9\n\t"
                "push r10\n\t"
                "push r11\n\t"
                "push r12\n\t"
                "push r13\n\t"
                "push r14\n\t"
                "push r15\n\t"
                "push r16\n\t"
                "push r17\n\t"
                "push r18\n\t"
                "push r19\n\t"
                "push r20\n\t"
                "push r21\n\t"
                "push r22\n\t"
                "push r23\n\t"
                "push %4\n\t"	// r24 start function arg - low byte (avr-gcc call convention)
                "push %5\n\t"	// r25 start function arg - high byte
                "push r26\n\t"
                "push r27\n\t"
                "push r28\n\t"
                "push r29\n\t"
                "push r30\n\t"
                "push r31\n\t"
    :
    :
                "r" ( (uint8_t) ( (uint16_t) &stopThread ) ),		// PC - low byte (stop function)
                "r" ( (uint8_t)( ( (uint16_t) &stopThread ) >> 8 ) ), 	// PC - high byte
                "r" ( (uint8_t) ( (uint16_t) functionPtr ) ),		// PC - low byte (start function)
                "r" ( (uint8_t)( ( (uint16_t) functionPtr ) >> 8 ) ),// PC - high byte
                "r" ( (uint8_t) ( (uint16_t) arg ) ),			// start function arg - low byte
                "r" ( (uint8_t)( ( (uint16_t) arg ) >> 8 ) )		// start function arg - high byte
            );

            thread[id].stackPtr  = (void *) SP;

            SP = stackPtr;
            result = TRUE;
        }
        else   //if we couldn't allocated the stack then free the id
        {
            tfree(id);
        }
    }
    sei();
    return result;
} // startThread

/*!
 * \brief Stop athe current thread.
 *
 * This absolutely does NOT free up resources held by the thread.  It only cleans up the stack space
 * and allow the TCB and stack partition to be recycled.
 *
 * Use at your own peril, and only if you REALLY know what you're doing
 */
void stopThread(void)
{
#ifdef KERNEL_DEBUG
    lockSemaphore(&usart1_tx_lock);
    transmitStringUSART1("stopTask\n");
    unlockSemaphore(&usart1_tx_lock);
#endif

    cli();
    thread[currentThread].state = STOPPED;
    tfree(currentThread);
    sfree(thread[currentThread].stackPtr);
    sei();

    while(1)
        asm volatile("nop"::);

} // stopTask

/*!
 * \brief Sleep the current thread for a given number of ticks.
 * \param ticks The number of ticks to sleep.
 *
 * A tick is defined as the time it takes for timer1 to overflow (~10ms by default).
 *
 * While the thread is sleeping its TCB state is set to SLEEPING.
 */
void sleep(uint16_t ticks)
{
    thread[currentThread].sleep = ticks1 + ticks;
    thread[currentThread].state = SLEEPING;

    while (thread[currentThread].sleep != 0)
        asm volatile("nop"::);
} // sleep

/*!
 * \brief Force the current thread to wait for one or more kernel events to fire
 * \param event the bit-field corresponding to the event(s) we're waiting for.
 * \see waitTimeout
 *
 * While waiting the TCB state is set to WAITING and the waitingFor flag is set to
 * the provided bit-field.
 *
 * As soon as one of the events being waited for fires the thread will wake up.
 *
 * When the thread wakes up the TCB's waitingFor flag is set to the bit-mask for
 * the event that caused it to wake up.
 */
void wait(Event event)
{
    cli();
    thread[currentThread].waitingFor = event;   // clear any old results
    thread[currentThread].state = WAITING;      // put the task to sleep
    sei();

    while(thread[currentThread].state == WAITING)
        asm volatile("nop"::);
}

// wait for an event, but wakeup prematurely if we wait too long
/*!
 * \brief Wait for a kernel event to fire, but only up to a certain number of ticks.
 * \param event The bit-field corresponding to the event(s) we're waiting for
 * \param ticks The maximum number of ticks we're willing to wait
 * \see wait
 *
 * The thread will wake up again if either one of the events being listened for fires
 * OR the timeout expires.
 *
 * If the thread woke up because of a timeout the TCB's waitingFor flag will be reset to zero.
 * Otherwise the waitingFor flag is set to the bit-mask for the event that fired.
 */
void waitTimeout(Event event, uint16_t ticks)
{
    cli();
    thread[currentThread].waitingFor = event;
    thread[currentThread].state = WAIT_TIMEOUT;
    thread[currentThread].sleep = ticks1 + ticks;
    sei();

    while(thread[currentThread].state == WAIT_TIMEOUT)
        asm volatile("nop"::);
}

/*!
 * \brief Called by ISRs to ake up tasks from either the WAITING or WAIT_TIMEOUT states
 * \param event The code for the event that was fired
 *
 * This function simply sets the pendingEvents flag.  The scheduler then worries about waking
 * the threads up during the next context-switch.
 */
void fireEvent(Event event)
{
    pendingEvents |= event;
}

/*!
 * \brief Initialize a semaphore with a given initial value
 * \param semaphore The semaphore to initialize
 * \param value The maximum number of locks the semaphore can have on it.  Should be 1 for a mutex
 *
 * Initially the semaphore is owned by the idle task
 */
void initSemaphore(volatile Semaphore *semaphore, uint8_t value)
{
    semaphore->value = -value;               // internally positive values indicate that the semaphore is locked
    semaphore->lockedBy = thread[IDLE].tid;  // initially semaphore is owned by the idle task
    semaphore->queueLength = 0;              // nobody's waiting in line yet
} // initSemaphore

/*!
 * \brief Lock a semaphore.  Will block until ownership passes to the calling thread.
 * \param semaphore
 *
 * If the semaphore is unlocked, ownership is given to the current thread.  Otherwise the thread
 * is set to the BLOCKED state and we wait until our turn.
 *
 * If we try to lock a semaphore we already own its value is increased, but we do not block.  This
 * means we can have nested locks of the same semaphore without causing a deadlock.
 *
 * Never, ever, ever call lockSemaphore from within an interrupt context.  Undefined results can happen,
 * since the calling thread is ambiguous, and we might wind up blocking inside an ISR.
 */
void lockSemaphore(volatile Semaphore *semaphore)
{
    cli();

    if (semaphore->value >= 0)   // the semaphore is currently locked
    {

        if(semaphore->lockedBy == thread[currentThread].tid)
        {
            semaphore->value++;
        }
        else
        {
            // insert in order of priority and niceness
            uint8_t x;

            for (x = 0; x < semaphore->queueLength; x++)
            {

                if ( thread[currentThread].priority > thread[ semaphore->waitList[x] ].priority &&
                        thread[currentThread].niceness < thread[ semaphore->waitList[x] ].niceness )
                {

                    uint8_t temp_task = semaphore->waitList[x];
                    uint8_t i;

                    for (i = x + 1; i < semaphore->queueLength; i++)
                    {

                        uint8_t swap = semaphore->waitList[i];
                        semaphore->waitList[i] = temp_task;
                        temp_task = swap;

                    }

                    semaphore->waitList[i] = temp_task;

                    break;

                }

            }

            semaphore->waitList[x] = currentThread;
            thread[currentThread].state = BLOCKED;

            semaphore->queueLength++;
        }
    }
    else // the semaphore is currently available
    {
        semaphore->value++;
        semaphore->lockedBy = thread[currentThread].tid;
    }

    sei();

    while (thread[currentThread].state == BLOCKED)
    {
        asm volatile("nop"::);
    }
} // lockSemaphore

/*!
 * \brief Unlock a semaphore.
 * \param semaphore The semaphore to unlock.
 *
 * This will decrement the semaphore's value REGARDLESS OF WHO ACTUALLY OWNS IT.
 * Be careful; only unlock things you own, unless you know what you're doing and
 * why you're doing it. (e.g. an ISR may want to unlock something, but the current
 * thread will be ambigious at such a time)
 */
void unlockSemaphore(volatile Semaphore *semaphore)
{

    cli();

    semaphore->value--;

    if (semaphore->value < 0 && semaphore->queueLength>0)
    {
        // we just unlocked the semaphore and there is 1+ thread waiting to lock it

        uint8_t x;
        thread[semaphore->waitList[0]].state = READY;
        semaphore->lockedBy = thread[semaphore->waitList[0]].tid;
        semaphore->queueLength--;

        for(x=1; x<=semaphore->queueLength; x++)
            semaphore->waitList[x-1] = semaphore->waitList[x];
    }
    else if (semaphore->value < 0)
    {
        // the semaphore is unlocked and there is no queue of other threads wanting it
        semaphore->lockedBy = IDLE;
    }

    sei();
} // unlockSemaphore

/*!
 * \brief Cede control of a semaphore completely; unlock it and pass it off to the next thread
 *        that is waiting to lock it.
 * \param semaphore The semaphore to unlock
 *
 * This should only be done when working with async while loops (using monitor threads) that
 * use a semaphore inside the loop that may not be unlocked before we execute the long-jump
 * to kick out of the loop
 */
void cedeSemaphore(volatile Semaphore* semaphore)
{
    while(semaphore->lockedBy == thread[currentThread].tid)
        unlockSemaphore(semaphore);
}

/*!
 * \brief Return whether or not we could lock the semaphore if we wanted to (i.e. is it either unlocked or already owned by us?)
 * \param semaphore The semaphore to lock
 * \return True if we can (re-)lock the semaphore
 */
BOOL tryLockSemaphore(volatile Semaphore *semaphore)
{
    uint8_t result = FALSE;

    if (semaphore->value < 0 || semaphore->lockedBy == thread[IDLE].tid || semaphore->lockedBy == thread[currentThread].tid)
    {
        result = TRUE;
    }

    return result;
} // tryLockSemaphore

/*!
 * \brief Initialize the rails that go between stack partitions
 */
void sinit(void)
{
    stack[RAIL0] = SCHECK;
    stack[RAIL1] = SCHECK;
    stack[RAIL2] = SCHECK;
    stack[RAIL3] = SCHECK;
    stack[RAIL4] = SCHECK;
    stack[RAIL5] = SCHECK;
    stack[RAIL6] = SCHECK;
    stack[RAIL7] = SCHECK;
    stack[RAIL8] = SCHECK;
} // sinit

/*!
 * \brief Allocate a block of stack space using a best-fit allocation policy
 * \param stackSize The minimum stack size we need to allocate
 * \return A pointer to the allocated stack, or NULL if no partitions were available
 */
void * salloc(uint16_t stackSize)
{
    void *result = NULL;
    uint8_t x;
    uint8_t bestFit = MAXIMUM_TASKS;
    uint16_t smallestDifference = 0xffff;

    // loop through all the available paritions and check them
    // we can stop if we ever find a perfect fit
    for (x = 0; x < MAXIMUM_TASKS && smallestDifference>0; x++)
    {
        if (stackSize <= partitionList[x].stackSize && partitionList[x].status == FREE)
        {
            // this is a possible candidate; see if it's better than the best we've found so far
            if (partitionList[x].stackSize - stackSize < smallestDifference)
            {
                smallestDifference = partitionList[x].stackSize - stackSize;
                bestFit = x;
            }
        }
    }

    // see if we found anything that fit
    if (bestFit < MAXIMUM_TASKS)
    {
        partitionList[bestFit].status = USED;
        result = partitionList[bestFit].stackPtr;
    }

    return result;
} // salloc

/*!
 * \brief Check that the rails between stack partitions are still intact
 * \return True if the rails are OK, otherwise false
 */
BOOL scheck(void)
{

    uint8_t result = TRUE;

    if (	stack[RAIL0] != SCHECK ||
            stack[RAIL1] != SCHECK ||
            stack[RAIL2] != SCHECK ||
            stack[RAIL3] != SCHECK ||
            stack[RAIL4] != SCHECK ||
            stack[RAIL5] != SCHECK ||
            stack[RAIL6] != SCHECK ||
            stack[RAIL7] != SCHECK ||
            stack[RAIL8] != SCHECK )
    {
        result = FALSE;
    }

    return result;
} // scheck

/*!
 * \brief Free a stack partition that was used by a thread
 * \param stackPtr Pointer to the start of the partition to free
 */
void sfree(void *stackPtr)
{
    uint8_t x;

    for (x = 0; x < MAXIMUM_TASKS; x++)
    {
        if ( stackPtr <= partitionList[x].stackPtr && stackPtr >= (partitionList[x].stackPtr - partitionList[x].stackSize) )
        {
            partitionList[x].status = FREE;
        }
    }
} // sfree

/*!
 * \brief Allocate a TCB for a new thread
 * \return The ID of the new thread
 */
uint8_t talloc(void)
{

    uint8_t result = MAXIMUM_TASKS;
    uint8_t x;
    if (totalThreads < MAXIMUM_TASKS)
    {

        for (x = 0; x < 8; x++)
        {
            if ( ( tidFree & (1 << x) ) == 0 )
            {
                tidFree |= (1 << x);
                totalThreads++;
                result = x;
                break;
            }
        }
    }

    return result;
} // talloc

/*!
 * \brief Free up a TCB for re-use
 * \param tid The ID of the task whose TCB we're recycling
 */
void tfree(uint8_t tid)
{
    tidFree &=  ( ~ (1 << tid) );
    totalThreads--;
} // tfree

/*!
 * \brief The IDLE task
 * \param arg ignored
 * At least one task must always be running, and this is the default if there are no other tasks available to schedule.
 *
 * This thread does nothing other than blink the MANAGE LED at 1Hz.
 */
void idle(void *arg)
{
    (void)arg;
    uint8_t i;

    for(;;)
    {
        changeLED( MANAGE_LED, LED_TOGGLE );
        for( i = 0; i < 100; i++ )
        {
            _delay_ms(100);
        }
    }
} // idle

/*!
 * \brief Use a priority scheduler with a niceness value to prevent starvation.
 *
 * Tasks of equal priority are round-robin scheduled.
 *
 * Called from inside the Timer1 ISR (i.e. once per tick)
 */
void schedule(void)
{
    uint8_t i;  // raw loop counter
    uint8_t x;  // the current task the loop considers; (last non-idle task # + i) MOD MAX_TASKS

    // because interrupts are enabled we need a local copy of events we're actually firing
    uint8_t firedEvents = pendingEvents;

    if ( ( currentThread < MAXIMUM_TASKS) && (thread[currentThread].state == RUNNING) )
    {
        thread[currentThread].state = READY;
    }
    else
    {
        currentThread = IDLE;
    }

    // walk through the threads and figured out what we should run next
    for (i = 0; i < MAXIMUM_TASKS; i++)
    {
        // start counting at the task just past the last one executed
        // this will ensure some degree of round-robin scheduling
        x = (lastNonIdleThread+i)%MAXIMUM_TASKS;

        if ( (tidFree & (1 << x)) != 0 )
        {
            if ((thread[x].state == WAITING || thread[x].state == WAIT_TIMEOUT) && (thread[x].waitingFor & firedEvents))
            {
                // wake up threads that were waiting for events
                thread[x].state = READY;
                thread[x].waitingFor = thread[x].waitingFor & firedEvents;
            }
            else if ((thread[x].state == SLEEPING || thread[x].state==WAIT_TIMEOUT) && (thread[x].sleep == ticks1))
            {
                // handle timeouts while waiting for events
                thread[x].waitingFor = 0;   // clear all events for WAIT_TIMEOUT
                thread[x].sleep = 0;
                thread[x].state = READY;
            }

            if (thread[x].state == READY)
            {

                if (thread[x].priority >= thread[currentThread].priority)
                {
                    currentThread = x;
                }
                else if (thread[x].priority < thread[currentThread].priority)
                {
                    if (thread[x].niceness < thread[currentThread].niceness)
                    {

                        thread[currentThread].niceness--;
                        currentThread = x;
                    }
                    else
                    {
                        thread[x].niceness--;
                    }

                }

            }
        }
    }

    // if we're not idling then update the last non-idle task ID
    if(currentThread!=IDLE)
        lastNonIdleThread = currentThread;

    thread[currentThread].niceness++;
    thread[currentThread].state = RUNNING;

    // clear the pending events
    // additional events may have been fired
    pendingEvents = pendingEvents ^ firedEvents;

#ifdef KERNEL_DEBUG
    // transmit T + the ID of the task we scheduled (Ti for idle)
    transmitStringUSART1("T");
    char c;
    switch(currentThread)
    {
    case IDLE:
        transmitStringUSART1("i");
        break;
    default:
        c = '0'+currentThread;
        transmitBufferUSART1(&c,1);
        break;
    }
#endif
} // schedule

/*!
 * \brief Start a monitor thread to run at regular intervals during the context-switch
 * \param functionPtr The function to call when executing the monitor thread
 * \param interval The number of ticks that must elapse between executions of the monitor thread
 * \return The TID of the new monitor thread
 */
uint8_t startMonitorThread( uint8_t (*functionPtr)(void), uint16_t interval )
{
    cli();
    // first off determine the first available index into which we can
    // insert a monitor thread
    uint8_t index = 0;
    for (index = 0; index<MAXIMUM_MONITOR_THREADS && monitorThread[index].tid!=NO_MONITOR_THREAD_SCHEDULED; index++);

    // if we couldn't start the thread return 0xff
    if(index >= MAXIMUM_MONITOR_THREADS)
    {
        sei();
        return NO_MONITOR_THREAD_SCHEDULED;
    }

    // initialize the new monitor thread
    monitorThread[index].execute = functionPtr;
    monitorThread[index].interval = interval;
    monitorThread[index].count = 0;
    monitorThread[index].tid = index;
    monitorThread[index].parentTid = thread[currentThread].tid;
    monitorThread[index].jmpBuf = NULL;

    monitorThread[index].level = thread[monitorThread[index].parentTid].monitorLevel;
    thread[monitorThread[index].parentTid].monitorLevel++;

    totalMonitorThreads++;

    sei();
    return monitorThread[index].tid;
}

/*!
 * \brief Start a monitor thread slaved to the current thread designed to kick out of a while-loop
 *        asynchronously
 * \param functionPtr The function to call when running the monitor thread
 * \param jmpBuf A jump buffer we use to long-jump out of the while loop
 * \return The TID of the monitor thread that we just created
 */
uint8_t startAsyncWhileThread( uint8_t (*functionPtr)(void), jmp_buf* jmpBuf )
{
    cli();

    // find out where we can insert a new monitor thread
    uint8_t index = 0;
    for (index = 0; index<MAXIMUM_MONITOR_THREADS && monitorThread[index].tid!=NO_MONITOR_THREAD_SCHEDULED; index++);

    if(index >= MAXIMUM_MONITOR_THREADS)
    {
        sei();
        return NO_MONITOR_THREAD_SCHEDULED;
    }

    monitorThread[index].execute = functionPtr;
    monitorThread[index].interval = 0;
    monitorThread[index].count = 0;
    monitorThread[index].tid = index;
    monitorThread[index].parentTid = thread[currentThread].tid;
    monitorThread[index].jmpBuf = jmpBuf;

    monitorThread[index].level = thread[monitorThread[index].parentTid].monitorLevel;
    thread[monitorThread[index].parentTid].monitorLevel++;

    totalMonitorThreads++;

    sei();
    return monitorThread[index].tid;
}

/*!
 * \brief Stop the monitor thread with a given TID
 * \param id The thread ID of the monitor thread to stop
 */
void stopMonitorThread( uint8_t id)
{
    uint8_t i;
    for(i=0; i<MAXIMUM_MONITOR_THREADS && monitorThread[i].tid!=id; i++);
    if(i == MAXIMUM_MONITOR_THREADS)
    {
        return;
    }


    uint8_t parentId = monitorThread[i].parentTid;

    thread[parentId].monitorLevel = monitorThread[i].level;

    // kill all monitor threads whose level is deeper than this one
    for (i=0; i<MAXIMUM_MONITOR_THREADS; i++)
    {
        if(monitorThread[i].parentTid == parentId && monitorThread[i].level >= thread[parentId].monitorLevel)
        {
            monitorThread[i].execute=NULL;
            monitorThread[i].tid = NO_MONITOR_THREAD_SCHEDULED;
            monitorThread[i].interval = 0;
            monitorThread[i].count = 0;
            monitorThread[i].parentTid = IDLE;
            //monitorThread[i].jmp_buf = NULL;

            totalMonitorThreads--;
        }
    }
}

/*!
 * \brief Schedule a single monitor thread to execute during a context-switch
 * \return A pointer to the MCB for the monitor to execute, or NULL if there is nothing to schedule
 */
volatile MCB* scheduleMonitorThreads()
{
    volatile MCB* ret = NULL;
    uint8_t i, x;
    for(i=1; i<=MAXIMUM_MONITOR_THREADS; i++)
    {
        x = (i+lastMonitorThread)% MAXIMUM_MONITOR_THREADS;

        monitorThread[x].count++;

        // if we can schedule this thread then return it
        if(monitorThread[x].count >= monitorThread[x].interval && monitorThread[x].tid != NO_MONITOR_THREAD_SCHEDULED && ret == NULL)
        {

            // we might be able to schedule this monitor if it does not require long-jumping, or if the parent thread is scheduled
            if (monitorThread[x].jmpBuf==NULL || monitorThread[x].parentTid == thread[currentThread].tid)
            {
                lastMonitorThread = x;
                monitorThread[x].count = 0;   // reset the count

                ret = &(monitorThread[x]);
            }
        }
    }

    return ret;
}

/*!
 * \brief Kernel panic; something catastrophic happened
 * \param errCode The error code that caused the panic
 *
 * This will kill everything and wait for a hard reset.  LEDs will flash
 * at about 2Hz and we transmit the error code in a busy-loop over USART1.
 *
 * All interrupts are disabled and USART0 is set to Rx mode to avoid accidentally
 * damaging servos by sending bad data to them
 */
void panic(uint8_t errCode)
{
    cli();          // ensure interrupts are turned off!
    setRxUSART0();  // make sure we can't send messages to the servos that might damage them

    powerLEDOn();
    playLEDOn();
    auxLEDOn();
    rxdLEDOn();
    txdLEDOn();
    programLEDOn();
    manageLEDOn();

    for(;;)
    {
        transmitByteUSART1(errCode);    // send the error code to the host PC

        changeLED(POWER_LED, LED_TOGGLE);
        changeLED(PLAY_LED, LED_TOGGLE);
        changeLED(AUX_LED, LED_TOGGLE);
        changeLED(RXD_LED, LED_TOGGLE);
        changeLED(TXD_LED, LED_TOGGLE);
        changeLED(PROGRAM_LED, LED_TOGGLE);
        changeLED(MANAGE_LED, LED_TOGGLE);

        _delay_ms(250);
    }
}

/*!
 * \brief Check the integrity of the stack and panic if we overflow
 * \param arg Ignored
 */
void monitorStack(void *arg)
{
    (void)arg;
    uint8_t memoryOK;
    for(;;)
    {
        memoryOK = scheck();
        if(!memoryOK)
            panic(ERR_STACK_OVERFLOW);

        sleep(100);
    }
} // memoryCheck

#ifdef KERNEL_DEBUG

// flash the AUX led
void testTask0(void *arg)
{
    for(;;)
    {
        changeLED(AUX_LED, LED_TOGGLE);
        sleep((uint8_t)arg);
    }
} // testTask0

// flash the PROGRAM led, but with a recursively-locked semaphore
void testTask1(void *arg)
{
    volatile Semaphore sem1;
    initSemaphore(&sem1, 1);

    for(;;)
    {
        lockSemaphore(&sem1);
        lockSemaphore(&sem1);
        changeLED(PROGRAM_LED, LED_TOGGLE);
        unlockSemaphore(&sem1);
        unlockSemaphore(&sem1);
        sleep((uint8_t)arg);
    }
} // testTask1

// flash the PLAY led
void testTask2(void *arg)
{
    for(;;)
    {
        changeLED(PLAY_LED, LED_TOGGLE);
        sleep((uint8_t)arg);
    }
} // testTask2

// toggle the POWER led
uint8_t testMonitor0()
{
    changeLED(POWER_LED, LED_TOGGLE);

    return PORT_LED & POWER_LED;
}

// toggle the PLAY led
uint8_t testMonitor1()
{
    changeLED(PLAY_LED, LED_TOGGLE);

    return PORT_LED & PLAY_LED;
}

#endif
