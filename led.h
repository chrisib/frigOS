/*
frigOS
Licensed under GPLv3
*/

#ifndef __LED_H__
#define __LED_H__

#include <avr/io.h>

#define PORT_LED  PORTC
#define DDR_LED   DDRC

/*!
 * \brief The pins the LEDs are connected to
 */
typedef enum
{
    PLAY_LED = (1 << PC6),
    PROGRAM_LED = (1 << PC5),
    MANAGE_LED = (1 << PC4),
    AUX_LED = (1 << PC3),
    RXD_LED = (1 << PC2),
    TXD_LED = (1 << PC1),
    POWER_LED = (1 << PC0)
} LEDs;

/*!
 * \brief Data direction register pins for each LED
 */
typedef enum
{
    DDR_PLAY_LED = (1 << DDC6),
    DDR_PROGRAM_LED = (1 << DDC5),
    DDR_MANAGE_LED = (1 << DDC4),
    DDR_AUX_LED = (1 << DDC3),
    DDR_RXD_LED = (1 << DDC2),
    DDR_TXD_LED = (1 << DDC1),
    DDR_POWER_LED = (1 << DDC0)
} LED_DDRs;

/*!
 * \brief The state for an LED (used to set the state)
 */
typedef enum
{
    LED_ON = 0,
    LED_OFF,
    LED_TOGGLE
} LED_STATE;

/*!
 * \brief Change an LED's state
 * \param led The LED to change
 * \param state The state to set the LED to
 */
static inline void changeLED( LEDs led, LED_STATE state )
{
    if ( state == LED_ON )
    {
        PORT_LED = PORT_LED & (~ led );
    }
    else if ( state == LED_OFF )
    {
        PORT_LED = PORT_LED | led;
    }
    else if ( state == LED_TOGGLE )
    {
        PORT_LED = PORT_LED ^ led;
    }
}

/*!
 * \brief Change the state of all LEDs
 * \param state The state to chang the LEDs to
 */
static inline void changeAllLEDs(LED_STATE state)
{
    changeLED(POWER_LED, state);
    changeLED(TXD_LED, state);
    changeLED(RXD_LED, state);
    changeLED(AUX_LED, state);
    changeLED(MANAGE_LED, state);
    changeLED(POWER_LED, state);
    changeLED(PLAY_LED, state);
}



// these used to be "static inline void ..." functions but
// have been replaced with macros to avoid unnecessary JSRs
// this should improve performance (very) slightly
#define playLEDOn()         PORT_LED &= ~PLAY_LED
#define playLEDOff(void)    PORT_LED |= PLAY_LED
#define programLEDOn()      PORT_LED &= ~PROGRAM_LED
#define programLEDOff()     PORT_LED |= PROGRAM_LED
#define manageLEDOn()       PORT_LED &= ~MANAGE_LED
#define manageLEDOff()      PORT_LED |= MANAGE_LED
#define auxLEDOn()          PORT_LED &= ~AUX_LED
#define auxLEDOff()         PORT_LED |= AUX_LED
#define rxdLEDOn()          PORT_LED &= ~RXD_LED
#define rxdLEDOff()         PORT_LED |= RXD_LED
#define txdLEDOn()          PORT_LED &= ~TXD_LED
#define txdLEDOff()         PORT_LED |= TXD_LED
#define powerLEDOn()        PORT_LED &= ~POWER_LED
#define powerLEDOff()       PORT_LED |= POWER_LED


void initLEDs(void);

#endif  //__LED_H__
