/*
frigOS
Licensed under GPLv3
*/

#ifndef DISABLE_ADC
#ifndef __ADC_H__
#define __ADC_H__

#include <stdint.h>

#define FALLEN_FRONT 550
#define FALLEN_BACK 225
#define FALL_THRESHOLD 1000

extern volatile uint8_t fallenBackFlag;
extern volatile uint8_t fallenFrontFlag;

extern volatile uint16_t accel_lower_limit;
extern volatile uint16_t accel_upper_limit;
extern volatile uint8_t accel_lateral_channel;
extern volatile uint8_t accel_range_set;

//extern volatile uint8_t battery[];
//extern volatile uint8_t accelerometer[];

void adcPause(void);
void adcResume(void);

void initADC(void);
//void transmitAccelData(void *arg);
//void testAccelerometer(void *arg);
//void initUpButton(void);
//void chargeBattery(void *arg);


//void checkFallen(void * arg);

//void readAccelerometer(void *arg);

#endif //__ADC_H__
#endif //DISABLE_ADC
