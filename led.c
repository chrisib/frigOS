/*
frigOS
Licensed under GPLv3
*/

#include "led.h"

/*!
 * \brief Initialize the CM-5 LEDs as pin outputs and initially set all of them to off
 */
void initLEDs(void)
{
    // set data direction output
    DDR_LED = DDR_LED | ( DDR_POWER_LED | DDR_PROGRAM_LED | DDR_PLAY_LED | DDR_AUX_LED | DDR_MANAGE_LED | DDR_RXD_LED | DDR_TXD_LED );

    // initially set all LEDs to off;
    PORT_LED = PORT_LED | ( POWER_LED | PROGRAM_LED | PLAY_LED | AUX_LED | MANAGE_LED | RXD_LED | TXD_LED );

} // initLEDs

