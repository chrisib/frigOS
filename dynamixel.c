#ifndef DISABLE_DYNAMIXEL

#include "kernel.h"
#include "protocol.h"
#include "serial.h"
#include "timer.h"
#include "led.h"
#include "shared_memory.h"
#include "avr/interrupt.h"
#include "dynamixel.h"

// calculate a packet checksum
// checksum is ~(id + instruction + length + parameter1 + ... + parameterN)
// (see dynamixel documentation)
/*!
 * \brief Calculate the checksum for a Dynamixel packet
 * \param id The ID of the destination
 * \param instruction The instruction the packet contains
 * \param parameters The parameters for the instruction
 * \param n Length of parameters
 * \return The calculated checksum, according to the Dynamixel serial protocol (see AX-12 datasheet, MX-28 datahseet, etc...)
 */
uint8_t calculateCheckSum(uint8_t id, uint8_t instruction, uint8_t const parameters[], uint8_t n)
{
    uint16_t sum = 0;
    uint8_t i;

    sum += id;
    sum += instruction;
    sum += (n+2);  // packet length is # of parameters +2

    for(i=0; i<n; i++)
        sum += parameters[i];

    return (uint8_t)(~sum);
}

/*!
 * \brief Create and set the checksum for a packet we are about to transmit
 * \param packet The packet to set the checksum for
 * \return The packet's checksum
 */
uint8_t createChecksum(DXL_Packet *packet)
{
    packet->checksum = calculateCheckSum(packet->id, packet->instrErr, packet->params, packet->paramLength);
    return packet->checksum;
}

/*!
 * \brief Send a command to the servo(s)
 * \param id The ID to send the command to
 * \param command The command to send
 * \param parameters The parameters for the command
 * \param paramLength The number of parameters
 * \param dest An optional buffer to store the response (should be set for any READ commands)
 *
 * If dest is non-null the response will be stored in the provided struct. Otherwise the response is silently
 * dropped.
 *
 * Dest will be all-zero if there was a timeout
 */
void sendCommand(uint8_t id, uint8_t command, uint8_t parameters[], uint8_t paramLength, DXL_Packet *dest)
{
    DXL_Packet cmdPacket;
    uint8_t i;

    cmdPacket.header1 = DXL_HEADER1;
    cmdPacket.header2 = DXL_HEADER2;
    cmdPacket.id = id;
    cmdPacket.instrErr = command;
    cmdPacket.paramLength = paramLength;
    cmdPacket.checksum = calculateCheckSum(id, command, parameters, paramLength);

    for(i=0; i<paramLength && i < DXL_MAX_PARAMS; i++)
        cmdPacket.params[i] = parameters[i];

    lockSemaphore(&usart0_lock);
    setTxUSART0();
    transmitPacketUSART0(&cmdPacket);
    unlockSemaphore(&usart0_lock);

    // wait until the packet is completely transmitted
    // and then revert to listening on USART0
    while(!txQueue_USART0.empty)
        wait(USART0_TX_EVENT);
    setRxUSART0();
    fireEvent(COMMAND_PACKET_SENT_EVENT);

    // wait for a response if necessary
    if(dest!=NULL)
    {
        waitTimeout(STATUS_PACKET_RECEIVED_EVENT, DXL_REPLY_TIMEOUT);
        //wait(STATUS_PACKET_RECEIVED_EVENT);

        if(thread[currentThread].waitingFor & STATUS_PACKET_RECEIVED_EVENT)
        {
            // we successfully received a response packet
            lockSemaphore(&responsePacketLock);

            dest->header1 = responsePacket.header1;
            dest->header2 = responsePacket.header2;
            dest->id = responsePacket.id;
            dest->instrErr = responsePacket.instrErr;
            dest->paramLength = responsePacket.paramLength;

            // copy the response packet to the provided buffer
            for(i=0; i<dest->paramLength && i < DXL_MAX_PARAMS; i++)
            {
                dest->params[i] = responsePacket.params[i];
            }

            dest->checksum = responsePacket.checksum;

            unlockSemaphore(&responsePacketLock);
        }
        else
        {
#ifdef DEBUG
            lockSemaphore(&usart1_tx_lock);
            transmitStringUSART1("command timed out\n");
            unlockSemaphore(&usart1_tx_lock);
#endif

            // we timed out
            dest->header1 = 0x00;
            dest->header2 = 0x00;
            dest->id = 0x00;
            dest->paramLength = 0x00;
            dest->checksum = 0x00;
            dest->instrErr = 0x00;
        }
    } // dest!=null
} // sendCommand

/*!
 * \brief Write a single byte to an address table
 * \param id The ID of the device to write to
 * \param address The address to set
 * \param data The data to write
 * \param dest Optional buffer to store the destination's response
 *
 * \see sendCommand
 */
void writeByte(uint8_t id, uint8_t address, uint8_t data, DXL_Packet *dest)
{
    uint8_t parameters[2];
    parameters[0] = address;
    parameters[1] = data;

    sendCommand(id, DXL_WRITE_DATA, parameters, 2, dest);
}

/*!
 * \brief Send a simple PING command to a device and return true/false if the ping was received and responded to
 * \param id The ID of the device to bing
 * \return True if we received a ping reply from the deivce, otherwise false
 */
BOOL ping(uint8_t id)
{
    DXL_Packet response;

    sendCommand(id, DXL_PING, NULL, 0, &response);

    if(validateResponsePacket(&response))
        return TRUE;
    else
        return FALSE;
}

/*!
 * \brief Read a set of contiguous bytes from a control table
 * \param id The ID to read from
 * \param startingAddress The first address to read from
 * \param numBytes The number of bytes to read
 * \param dest A buffer to store the response in
 * Result is stired in dest->params
 */
void readBytes(uint8_t id, uint8_t startingAddress, uint8_t numBytes, DXL_Packet *dest)
{
    uint8_t payload[2];

    payload[0] = startingAddress;
    payload[1] = numBytes;

    sendCommand(id, DXL_READ_DATA, payload, 0x02, dest);
}

/*!
 * \brief Validate a response packet; Check the error code and checksum
 * \param packet The response packet to check
 * \return True if the packet was valid and well-formed, otherwise false
 */
BOOL validateResponsePacket(DXL_Packet *packet)
{
    uint8_t checksum = calculateCheckSum(packet->id, packet->instrErr, packet->params, packet->paramLength);
    if(packet->header1 == DXL_HEADER1 && packet->header2 == DXL_HEADER2 && checksum == packet->checksum)
        return TRUE;
    else
        return FALSE;
}

#endif // DISABLE_DYNAMIXEL
