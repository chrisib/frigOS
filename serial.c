/*
frigOS
Licensed under GPLv3
*/

#include "serial.h"
#include "adc.h"
#include "ax12.h"
#include "axs1.h"
#include "cm5.h"
#include "led.h"
#include "protocol.h"
#include "kernel.h"
#include "shared_memory.h"

#include <avr/interrupt.h>
#include <util/delay.h>

// START TODO - these can be removed eventually
uint8_t reply_status __attribute__((deprecated("")));
uint8_t reply_valid __attribute__((deprecated("")));
uint8_t reply[4] __attribute__((deprecated("")));
int servoIdx __attribute__((deprecated("")));
// END TODO

/*!
 * \brief Initialize the RS485 communcation; needed for USART0 if we want to communcate with the servos
 */
void initRS485(void)
{
    // set data direction output
    // RS485 half duplex
    DDRE |= (1 << DDE3) | (1 << DDE2);

} // initRS485


/**********************************************************************************/
/** ENQUEUE/DEQUEUE FUNCTIONS *****************************************************/
/**********************************************************************************/

/*!
 * \brief Add a byte to the end of the queue
 * \param data The data to add
 * \param queue The queue to add the data to.
 *
 * If the queue is full the data is silently dropped
 */
void enqueue(uint8_t data, volatile IO_Queue *queue)
{
    if(!queue->full)
    {
        queue->buffer[queue->in] = data;
        queue->in = (queue->in + 1) % queue->maxLength;
        queue->queueLength++;
        queue->empty = FALSE;

        // check if the queue is full
        if (((queue->in+1) % queue->maxLength) == queue->out)
        {
            queue->full = TRUE;
        }
    }

    // dequeue a byte and transmit it if necessary
    if (queue->txOn != TX_ON_NONE && queue->txOnEnqueue && !queue->empty)
    {
        switch(queue->txOn)
        {
        case TX_ON_USART0:
            data = dequeue(queue);

            // send the byte
            queue->txOnEnqueue= FALSE;

            // wait for the register to be empty
            while ( !( UCSR0A & (1 << UDRE0) ) )
                asm volatile("nop"::);
            UDR0 = data;

            break;

        case TX_ON_USART1:
            txdLEDOn();

            data = dequeue(queue);

            queue->txOnEnqueue = FALSE;

            // wait for the register to be empty
            while ( !( UCSR1A & (1 << UDRE1) ) )
                asm volatile("nop"::);
            UDR1 = data;

            txdLEDOff();

            break;

        default:
            break;
        }
    }
}

/*!
 * \brief Remove a byte from the front of the queue and return it
 * \param queue The queue to remove the data from
 * \return The data we removed or 0x00 if the queue was empty.
 *
 * Since 0x00 may in fact be useful data, we should check queue->isEmpty before calling dequeue
 */
uint8_t dequeue(volatile IO_Queue *queue)
{
    if(queue->empty)
        return 0x00;    // we don't care; the caller should check if the queue is empty before calling dequeue

    uint8_t data = queue->buffer[queue->out];
    queue->out = (queue->out+1) % queue->maxLength;
    queue->queueLength--;
    queue->full = FALSE;

    if(queue->in == queue->out)
        queue->empty = TRUE;

    return data;
}

/*!
 * \brief Convert a half-byte to a single char
 * \param nibble The half-byte to convert (in the lower 4 bits)
 * \return A hex char corresponding to the lower nibble
 */
char nibble2char(uint8_t nibble)
{
    nibble = ((nibble << 4) >> 4) & 0x0f;
    if(nibble<10)
        return '0'+nibble;
    else
        return 'a'+nibble-10;
}

/*!
 * \brief Convert a single byte to a hex string
 * \param src The byte to convert
 * \param dest A char array to write the result to.  Must have at least 2 chars in it
 *
 * This function does not add any null terminators; it just writes 2 chars to the dest array
 */
void byte2hex(uint8_t src, char dest[])
{
    uint8_t lo4 = src & 0x0f;
    uint8_t hi4 = (src & 0xf0) >> 4;

    dest[0] = nibble2char(hi4);
    dest[1] = nibble2char(lo4);
}
