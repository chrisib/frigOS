/*
frigOS
Licensed under GPLv3
*/
#ifndef __KERNEL_H__
#define __KERNEL_H__

#include <stdint.h>
#include <setjmp.h>
#include "defs.h"

#ifndef MAXIMUM_TASKS
/*!
 * \brief The maximum number of threads that can be running concurrently, including the idle thread
 */
#   define MAXIMUM_TASKS 8
#endif

#ifndef MAXIMUM_MONITOR_THREADS
/*!
 * \brief The maximum number of monitor threads that can be running concurrently
 */
#   define MAXIMUM_MONITOR_THREADS    32
#endif

/*!
 * \brief Value indicating that there is no monitor thread to be executed during the current timer interrupt
 */
#define NO_MONITOR_THREAD_SCHEDULED   0xff

// the stack is partitioned into MAXUMUM_TASKS blocks of varying size
// the sized are measured in kB
#ifndef MINIMUM_STACK_SIZE
/*!
 * \brief Minimum possible stack size for a thread
 */
#   define MINIMUM_STACK_SIZE 256
#endif

#ifndef MEDIUM_STACK_SIZE
/*!
 * \brief Stack size for medium-sized threads
 */
#    define MEDIUM_STACK_SIZE 256
#endif

#ifndef MAXIMUM_STACK_SIZE
/*!
 * \brief Maximum possible stack size for a thread
 */
#    define MAXIMUM_STACK_SIZE 512
#endif

/*!
 * \brief Stack size of the idle task
 */
#define IDLE_STACK_SIZE 128

/*!
 * \brief Size of the rails around each stack partition (used to detect overflows)
 */
#define RAIL_STACK_SIZE 9

// total stack size with the appropriate partition sizes
#ifndef STACK_SIZE
/*!
 * \brief Total size of all partitions + rails
 */
#   define STACK_SIZE (IDLE_STACK_SIZE) + (MINIMUM_STACK_SIZE*4) + (MEDIUM_STACK_SIZE*2) + (MAXIMUM_STACK_SIZE) + (RAIL_STACK_SIZE)
#endif

//2 kb

#define RAIL0 STACK_SIZE - 1
#define RAIL1 STACK_SIZE - 2 - IDLE_STACK_SIZE
#define RAIL2 STACK_SIZE - 2 - IDLE_STACK_SIZE - 1 - MINIMUM_STACK_SIZE
#define RAIL3 STACK_SIZE - 2 - IDLE_STACK_SIZE - 1 - MINIMUM_STACK_SIZE*2 - 1
#define RAIL4 STACK_SIZE - 2 - IDLE_STACK_SIZE - 1 - MINIMUM_STACK_SIZE*3 - 2
#define RAIL5 STACK_SIZE - 2 - IDLE_STACK_SIZE - 1 - MINIMUM_STACK_SIZE*4 - 3
#define RAIL6 STACK_SIZE - 2 - IDLE_STACK_SIZE - 1 - MINIMUM_STACK_SIZE*4 - 4 - MEDIUM_STACK_SIZE
#define RAIL7 STACK_SIZE - 2 - IDLE_STACK_SIZE - 1 - MINIMUM_STACK_SIZE*4 - 4 - MEDIUM_STACK_SIZE*2 - 1
#define RAIL8 0
#define SCHECK 0xff

/*!
 * \brief Task IDs; used to index through the stack partitions
 * \see MAXIMUM_TASKS
 */
enum task_id {IDLE, TID1, TID2, TID3, TID4, TID5, TID6, TID7};

/*!
 * \brief Thread states
 * - Running: the current, active thread
 * - Ready: can be scheduled to run at the next timer interrupt
 * - Blocked: thread is blocked and waiting for a semaphore
 * - Stopped: the thread is inactive
 * - Sleeping: the thread is sleeping for a fixed interval
 * - Waiting: the thread is waiting for an event to fire
 * - Wait-Timeout: the thread is waiting for an event to fire, but will time out eventually and go back to Ready
 */
enum task_state {RUNNING, READY, BLOCKED, STOPPED, SLEEPING, WAITING, WAIT_TIMEOUT};

/*!
 * \brief Thread priority states
 */
enum task_prioriy {IDLE_PRIORITY, MINIMUM_PRIORITY, MEDIUM_PRIORITY, MAXIMUM_PRIORITY};

/*!
 * \brief Flag indicating if each stack partition is used or not
 */
enum partition_status {FREE = 1, USED = 0};

/*!
 * \brief Kernel error bit field values
 */
enum kernel_err
{
    ERR_STACK_OVERFLOW = (1<<0),    // a thread's stack overflowed
    ERR_USART0 = (1<<1),            // critical error with USART0
    ERR_USART1 = (1<<2)             // critical error with USART1
    // TODO: other error codes
};

/*!
 * \brief Events that can be fired from the kernel.
 * We can support up to 16 events, since the event flag is a 16-bit field.
 * Not all events are necessarily implemented yet
 */
typedef enum event_t
{
    // interrupt events
    USART0_RX_EVENT = (1<<0),        // fires from USART0 Rx ISR
    USART0_TX_EVENT = (1<<1),        // fires from USART0 Tx ISR
    USART1_RX_EVENT = (1<<2),        // fires from USART1 Rx ISR
    USART1_TX_EVENT = (1<<3),        // fires from USART1 Tx ISR
    A2D_COMPLETE_EVENT = (1<<4),     // fires from A2D Complete ISR

    // NOT IMPLEMENTED TIMER1_OVEFLOW_EVENT = (1<<5),   // fires from timer 1 overflow ISR
    TIMER2_OVERFLOW_EVENT = (1<<6),  // fires from timer 2 overflow ISR
    TIMER3_OVERFLOW_EVENT = (1<<7),  // fires from timer 3 overflow ISR

    // thread-controlled events
    COMMAND_PACKET_SENT_EVENT = (1<<8),      // fires when a command packet is sent to the servos
    STATUS_PACKET_RECEIVED_EVENT = (1<<9),   // fires when a complete status packet from the servos is received

    // button events
    UP_BUTTON_PRESSED_EVENT = (1<<10),
    DOWN_BUTTON_PRESSED_EVENT = (1<<11),
    LEFT_BUTTON_PRESSED_EVENT = (1<<12),
    RIGHT_BUTTON_PRESSED_EVENT = (1<<13),
    // NOT IMPLEMENTED START_BUTTON_PRESSED_EVENT = (1<<14),

    // User-defined event; may be used for future expansion
    USER_DEF_EVENT1 = (1<<15)
} Event;

/*!
 * \brief Thread control block. Every task/thread is assigned one of these to track its state, stack pointer, etc...
 */
typedef struct tcb
{
    uint8_t tid;            // thread ID; unique to each thread running, but may be recycled if a thread is stopped and a new one started
    uint8_t state;          // the thread's state.  see enum task_state
    uint8_t priority;       // the thread's priority. see enum task_priority
    int8_t niceness;        // the thread's niceness; this is used by the scheduler to prevent high priority tasks from starving-out lower-priority ones
    void *stackPtr;         // pointer to this thread's stack partition
    uint16_t sleep;         // used by the sleep and waitTimeout functions to count how long the thread is sleeping
    uint16_t waitingFor;    // used by wait and fireEvent functions to track what events the thread is waiting for
    uint8_t monitorLevel;   // used by startMonitorThread to allow nested asynchronous loops
} TCB;

/*!
 * \brief Monitor thread control block
 * \see TCB
 * Monitor threads are like normal threads, only they do not use their own stack space and are not scheduled
 * using the normal scheduler.  Instead, one or more monitor threads may execute during the timer interrupt
 * when we change tasks.  The monitor thread uses the top of the scheduled task's stack and returns
 * before the newly scheduled task is executed.
 *
 * Can be used in 2 modes:
 * - continuous monitor: check at regular intervals, using global variables for I/O
 * - asynchronous loop condition: use setjmp/longjmp to break out of the parent thread's loop
 */
typedef struct mcb
{
    uint8_t (*execute)(void);   // function to execute
    uint16_t interval;          // ms to elapse between executions
    uint16_t count;             // number of ms that have elapsed
    uint8_t tid;                // unique monitor thread id
    uint8_t parentTid;          // id of parent thread
    uint8_t level;              // the monitorLevel of the parent thread when this monitor was started
    jmp_buf* jmpBuf;            // long-jump buffer used for asynch while looping
} MCB;

/*!
 * \brief Semaphore/mutex implementation.  Used to coordinate between threads
 */
typedef struct semaphore
{
    int8_t value;                       // negative values indicate the semaphore is unlocked; values >=0 indicate the semaphore is locked
    uint8_t waitList[MAXIMUM_TASKS];    // a queue of threads waiting to lock this semaphore
    uint8_t queueLength;                // the length of the wait queue
    uint8_t lockedBy;                   // the TID of the thread that currently owns the lock; set to IDLE of the semaphore is unlocked
} Semaphore;

/*!
 * \brief Stack partition; every task has a partition assigned to it for its own stack
 */
typedef struct partition
{
    void *stackPtr;         // pointer to the start of the partition's allocated memory
    uint16_t stackSize;     // size of the stack partition
    uint8_t status;         // USED or FREE
} Partition;


// keep arrays of control blocks for threads and monitor threads
extern volatile TCB thread[];
extern volatile MCB monitorThread[];

// the entire partitioned stack space is stored as an array so we can check rails
extern volatile uint8_t stack[];

// the currently executing thread; should be treated as read-only except by the scheduler
extern volatile uint8_t currentThread;

// total number of threads currently running
extern volatile uint8_t totalThreads;

// ID of the next available free TCB slot
extern volatile uint8_t tidFree;

// total number of running monitor threads
extern volatile uint8_t totalMonitorThreads;


/* initialization functions */
void boot(void);
void initKernel(void);
void initMonitorThreads(void);
void printBootBanner(void);


/* standard thread controls */
uint8_t startThread(void (*functionPtr)(void *arg), void *arg, uint8_t priority, uint16_t stackSize);
void stopThread(void);

void sleep(uint16_t ticks);
void schedule(void);


/* event controls */
void wait(Event event);
void waitTimeout(Event event, uint16_t ticks);
void fireEvent(Event event);


/* monitor thread controls */
uint8_t startMonitorThread( uint8_t (*functionPtr)(void), uint16_t interval );
uint8_t startAsyncWhileThread( uint8_t (*functionPtr)(void), jmp_buf* jmpBuf );
void stopMonitorThread( uint8_t id);
volatile MCB* scheduleMonitorThreads(void);


/* semaphore controls */
void initSemaphore(volatile Semaphore *semaphore, uint8_t value);
void lockSemaphore(volatile Semaphore *semaphore);
void unlockSemaphore(volatile Semaphore *semaphore);
void cedeSemaphore(volatile Semaphore *semaphore);
BOOL tryLockSemaphore(volatile Semaphore *semaphore);


/* stack initialization functions */
// these generally aren't needed outside kernel.c
// see kernel.c for function descriptions
void sinit(void);
void *salloc(uint16_t stackSize);
BOOL scheck(void);
void sfree(void *stackPtr);
uint8_t talloc(void);
void tfree(uint8_t tid);


/* threadable functions */
void idle(void *arg);
void monitorStack(void *arg);

// The Kernel Panic function -- call this if things have gone to shit and you might damage the hardware
void panic(uint8_t errCode);


#ifdef KERNEL_DEBUG
// sample threads and monitors that can be used for kernel debugging
void testTask0(void *arg);
void testTask1(void *arg);
void testTask2(void *arg);

uint8_t testMonitor0(void);
uint8_t testMonitor1(void);
#endif

#endif
