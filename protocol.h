/*
frigOS
Licensed under GPLv3
*/
#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

#include <stdint.h>

enum COMMANDS {

    CMD_HEADER = 0xff,
    CMD_LINEAR = 0x01,
    CMD_SINUSOIDAL = 0x02,
    CMD_GET_POSITION = 0x03,
    CMD_SET_POSITION_AND_SPEED = 0x04,
    CMD_MOTOR_OFF = 0x05,
    CMD_MOTOR_ON = 0x06,
    CMD_PING = 0x07,
    CMD_WRITE_MOTION = 0x08,
    CMD_PLAY_MOTION = 0x09,
    CMD_STOP_MOTION = 0x0a,
    CMD_ACCELEROMETER = 0x0b,
    CMD_BALANCE = 0x0c,
    CMD_RESET = 0x0d,
    CMD_MOVE_LOAD_CHECK = 0x0e,

    CMD_COMPENSATION = 0x0f,
    CMD_ACCEL_INIT = 0x10,
    CMD_CHECK_FALLEN = 0x11

};

#define CMD_DEFAULT_LENGTH 3
#define CMD_INTERPOLATE_LENGTH (NUMBER_OF_SERVOS + 3)
#define CMD_SET_POSITION_AND_SPEED_LENGTH 5
#define CMD_PLAY_LENGTH 5
#define CMD_STOP_LENGTH 2
#define CMD_ACCELEROMETER_LENGTH 2
#define CMD_RESET_LENGTH 4
#define CMD_MOVE_LOAD_CHECK_LENGTH 4
#define CMD_ACCEL_INIT_LENGTH 7
#define CMD_CHECK_FALLEN_LENGTH 2
#define CMD_COMPENSATION_LENGTH (NUMBER_OF_SERVOS + 2)

uint8_t protocolCheckSum(uint8_t const buffer[], uint8_t buffer_length);
void protocolSuccess(void);
void protocolFail(void);

#endif
